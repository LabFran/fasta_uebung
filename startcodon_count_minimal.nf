nextflow.enable.dsl=2

process startcodon_count {
  publishDir "/tmp/", mode: 'copy', overwrite: true
  input:
    path infile
    val word
  output:
    path "${infile}.wordcount", emit: wordcount
  script:
    """
    grep -o -i ${word} ${infile} > grepresult
    cat grepresult | wc -l > ${infile}.wordcount
    """
}

workflow {
  inchannel = channel.fromPath("../data/sequences.fasta")
  startcodon_count(inchannel, "ATG")
}
